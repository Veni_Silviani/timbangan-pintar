<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;
use App\Http\Resources\BarangResource;
use App\Models\Barang;
use App\Http\Controllers\Api\BarangController;
use App\Http\Controllers\Api\DeviceController;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });
Route::post('/register', [App\Http\Controllers\Api\AuthController::class, 'register']);
Route::post('/login', [App\Http\Controllers\Api\AuthController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function () {
    Route::get('/profile', function() {
        return auth()->user()->id;
    });
    Route::post('/logout', [App\Http\Controllers\Api\AuthController::class, 'logout']);
    Route::resource('barang', BarangController::class)->except(['create','edit']);
    Route::resource('device', DeviceController::class)->except(['create','edit']);
    Route::post('/kepemilikan', [App\Http\Controllers\Api\KepemilikanController::class, 'store']);
    Route::get('/kepemilikan', [App\Http\Controllers\Api\KepemilikanController::class, 'show']);
    Route::delete('/kepemilikan/{id}', [App\Http\Controllers\Api\KepemilikanController::class, 'destroy']);
    Route::post('/penimbangan', [App\Http\Controllers\Api\PenimbanganController::class, 'store']);
    Route::get('/penimbangan/device/{id}',[App\Http\Controllers\Api\PenimbanganController::class, 'device']);
    Route::get('/penimbangan/barang/{id}',[App\Http\Controllers\Api\PenimbanganController::class, 'barang']);
    Route::delete('/penimbangan/{id}', [App\Http\Controllers\Api\PenimbanganController::class, 'destroy']);
});

// Route::get('/tampilkan', function () {
//     return BarangResource::collection(Barang::all());
// });
// Route::get('/tampilkan/{id}', function ($id) {
//     return new  BarangResource(Barang::findOrFail($id));
// });