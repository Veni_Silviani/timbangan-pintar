<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Penimbangan extends Model
{
    protected $table = "penimbangan";
    protected $fillable = [
        "berat",
        "device_id",
        "barang_id"
    ];

    public function device() {
        return $this->belongsTo(Device::class, "device_id");
    }
    public function barang() {
        return $this->belongsTo(Barang::class, "barang_id");
    }
}
