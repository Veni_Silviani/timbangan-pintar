<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Device extends Model
{
    protected $table = "device";
    protected $fillable = ['name'];
    public function penimbangan() {
        return $this->hasMany(Penimbangan::class);
    }
    public function kepemilikan() {
        return $this->hasMany(Kepemilikan::class);
    }
}
