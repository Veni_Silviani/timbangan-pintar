<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class PenimbanganResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'device_id' => $this->device_id,
            'barang_id' => $this->barang_id,
            'name' => $this->name,
            'berat' => $this->berat,
            'created_at' => $this->created_at,
            'updated_at' => $this->updated_at
        ];
    }
}
