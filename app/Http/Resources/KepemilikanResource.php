<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class KepemilikanResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'device_id' => $this->device_id,
            'device_name' => $this->name
        ];
    }
}
