<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Barang;
use App\Http\Resources\BarangResource;
use Validator;


class BarangController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $data = Barang::latest()->get();
        return response()->json([BarangResource::collection($data), 'Barang Fetched']);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'rfid' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors());
        }

        $barang = Barang::create([
            'name' => $request->name,
            'rfid' => $request->rfid
        ]);

        return response()->json(['Barang created successfully', new BarangResource($barang)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $barang = Barang::find($id);
        if (is_null($barang)) {
            return response()->json(['Data Not Found', 404]);
        }
        return response()->json([new BarangResource($barang)]);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(),[
            'name' => 'required',
            'rfid' => 'required'
        ]);

        if ($validator->fails()) {
            return response()->json($validator->errors());
        }

        $barang = Barang::find($id);
        $barang->name = $request->name;
        $barang->rfid =$request->rfid;
        $barang->save();

        return response()->json(['Barang updated successfully', new BarangResource($barang)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $barang = Barang::find($id);
        $barang->delete();

        return response()->json('Barang deleted successfully');
    }
}
