<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Penimbangan;
use App\Http\Resources\PenimbanganResource;
use Validator;


class PenimbanganController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'berat' => 'required',
            'device_id'  => 'required',
            'barang_id' => 'required'
        ]);

        if ($validator->fails()){
            return response()->json($validator->errors());
        }

        $penimbangan = Penimbangan::create([
            'berat' => $request->berat,
            'device_id' => $request->device_id,
            'barang_id' => $request->barang_id
        ]);

        return response()->json(['Penimbangan created successfully', new PenimbanganResource($penimbangan)]);

    }

    public function device($id) {
        $penimbangan = Penimbangan::join('barang', 'barang_id', '=', 'barang.id')
                                    ->select('penimbangan.*', 'barang.name')->where('device_id', $id)->get();
        return response()->json([PenimbanganResource::collection($penimbangan)]);
    }
    public function barang($id) {
        $penimbangan = Penimbangan::join('barang', 'barang_id', '=', 'barang.id')
                                    ->select('penimbangan.*', 'barang.name')->where('barang_id', $id)->get();
        return response()->json([PenimbanganResource::collection($penimbangan)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $penimbangan = Penimbangan::find($id);
        $penimbangan->delete();
        return response()->json('Penimbangan deleted successfully');
    }
}
