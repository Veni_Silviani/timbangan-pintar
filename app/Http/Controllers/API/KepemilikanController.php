<?php

namespace App\Http\Controllers\API;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Kepemilikan;
use App\Http\Resources\KepemilikanResource;

class KepemilikanController extends Controller
{

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $id_user = auth()->user()->id;
        $kepemilikan = Kepemilikan::create([
            'user_id' => $id_user,
            'device_id' => $request->device_id
        ]);

        return response()->json(['Kepemilikan created successfully', new KepemilikanResource($kepemilikan)]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $id_user = auth()->user()->id;
        $kepemilikan = Kepemilikan::join('device', 'device_id', '=', 'device.id')
                                    ->select('kepemilikan.*', 'device.name')->where('user_id', $id_user)->get();
        return response()->json([KepemilikanResource::collection($kepemilikan)]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kepemilikan = Kepemilikan::find($id);
        $kepemilikan->delete();
        return response()->json('Kepemilikan deleted successfully');
    }
}
